<?php
session_start();

require("include/config.php");
require("include/db.php");
require("include/functions.php");
require("include/gump.class.php");
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <!-- google captcha -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
  <script type="text/javascript" src="node_modules\jquery-validation\dist\jquery.validate.js"></script>
  <link href="fullcalendar/lib/fullcalendar.min.css" rel="stylesheet" />
  <link href="fullcalendar/scheduler.min.css" rel="stylesheet" />
  <script src="fullcalendar/lib/jquery.min.js"></script>
  <script src="fullcalendar/lib/moment.min.js"></script>
  <script src="fullcalendar/lib/fullcalendar.min.js"></script>
  <script src="fullcalendar/scheduler.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <title>Car Workshop</title>
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top justify-content-center">
    <div class="container" id="navi">
      <a class="navbar-brand em-text" href="index.php">
        <img src="img/logo.png" width="30" height="30" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="#navbarsExample09"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample09">

        <ul class="nav navbar-nav navbar-right flex-row justify-content-start ml-auto">
          <?php
          if(isset($_SESSION['user_id'])) {
            $name = get_user_name($_SESSION['user_id']); 
            echo "<li><a href=\"logout.php\" class=\"nav-link\">Logout</a></li>";  		          
          }                               
          ?>
        </ul>
      </div>
    </div>
  </nav>

  <?php	

  if(isset($_SESSION['user_id']) && isset($_SESSION['role_id']) && $_SESSION['role_id'] == 2) {
    
  echo <<<EOT
  
  <nav class="navbar fixed-top navbar-light navbar-left bg-light justify-content-center" id="navi2">
    <div class="container">
      <ul class="nav  mr-auto">
        <li>
          <p class="navbar-text navbar-left"><strong>Welcome $name</strong></p>
        </li>
      </ul>
      
     </div>
  </nav>

EOT;
  }
  
  ?>
    <section id="middle" style="margin-top:50px;">
      <div class="container" id="pageContent">
        <?php
if (!isset($_POST['insertproblem'])) {
  header("location: index.php");
}
else {

  $validator = new GUMP();

  $user_id = mysqli_real_escape_string($connection, $_SESSION['user_id']);
  $plate_num = mysqli_real_escape_string($connection, $_POST['plate_num']);
  $car_year = mysqli_real_escape_string($connection, $_POST['car_year']);
  $brand = mysqli_real_escape_string($connection, $_POST['brand']);
  $model = mysqli_real_escape_string($connection, $_POST['model']);
  $user_comment = mysqli_real_escape_string($connection, $_POST['user_comment']);

  foreach($_POST['problems'] as $single) {
    $problem[]=mysqli_real_escape_string($connection, $single);
  }
  //change from array to string
  $problems = implode(", ", $problem);
  $_POST = array(
    'userid'     => $user_id,
    'platenum'   => $plate_num,
    'caryear' 	 => $car_year,
    'brand'      => $brand,
    'model' 	   => $model,
    'usercomment'=> $user_comment
  );

  $_POST = $validator->sanitize($_POST);

  $rules = array(
    'userid'     => 'required|min_len,1|max_len,3',
    'platenum'   => 'required|alpha_space|min_len,5|max_len,10',
    'caryear' 	 => 'required|numeric|min_len,4|max_len,4',
    'brand'      => 'required',
    'model' 	   => 'required',
    'usercomment'=> 'required|max_len,1000'
  );

  $filters = array(
    'userid'     => 'trim|sanitize_string',
    'platenum'   => 'trim|sanitize_string',
    'caryear' 	 => 'trim|sanitize_string',
    'brand'      => 'trim|sanitize_string',
    'model' 	   => 'trim|sanitize_string',
    'usercomment'=> 'trim|sanitize_string'
  );


  $_POST = $validator->filter($_POST, $filters);


  $validated = $validator->validate(
      $_POST, $rules
  );

  if($validated === TRUE) {

    // checking if car with that plate num is already in DB
    $sql = "SELECT * from cars WHERE plate_num='$plate_num'";
    $result = mysqli_query($connection,$sql) or die(mysql_error($connection));
    // if yes, that check if its in service or its done
    if ($result->num_rows>0) {
        $problem_exist = array();
        while($row = $result->fetch_assoc()) {
          $problem_exist[] = $row['problem_id'];    
        } 
      //change from array to string
      $problem_exist = implode(", ", $problem_exist);
      // checking if service for that car is already done
      $sql_exist = "SELECT * from problems WHERE problem_status_id!='5' AND problem_id IN ($problem_exist)";

      $result = mysqli_query($connection,$sql_exist) or die(mysql_error($connection));
      //if its still in service return Error message
    if ($result->num_rows>0) {
      $_SESSION['message'] ="<div class=\"alert alert-warning\" role=\"alert\">
      <h4 class=\"alert-heading\">Warning!</h4>
      <p>Car with enetred plate number is still in service or in not belongs to you.</p>
      <p class=\"mb-0\">When the service is done, car can be scheduled for new service.</p>

      </div>";

      header("location: index.php?#pagemessage.php");
    }   
          //if its only in DB, but service is already done, user can set up new service
    elseif($result->num_rows===0) {
      //inserting new problem
      $problem_id = insert_problem($user_id,$user_comment, $connection);
      //after problem is inserted using new id for cars table
      // insertion to cars table
      $car_id = insert_car($problem_id, $plate_num, $brand, $model, $car_year, $connection);
      //selecting values from service table to be used in for each problem
      $price_time_temp = get_time_price($problems,$problem_id,$connection);
      $problem_time_temp = $price_time_temp[0];
      $service_price_temp = $price_time_temp[1];

      //converting time to int to be used in selectAllow
      $format_time = format_time($problem_time_temp);
      $problem_time_fl = $format_time[0];
      $problem_time_rounded = $format_time[1];
      $problem_time_temp1 = $format_time[2];
      //updating problems with missing data
      $sql_upd = "UPDATE problems SET car_id ='$car_id',problem_total_price= '$service_price_temp', problem_total_time ='$problem_time_temp1' WHERE problem_id ='$problem_id' AND user_id ='$user_id' ";
      mysqli_query($connection, $sql_upd);
    }
    }
    elseif ($result->num_rows==0) {
      //inserting problem
      $problem_id = insert_problem($user_id,$user_comment, $connection);
      //after problem is inserted using new id for cars table
      // insertion to cars table
      $car_id = insert_car($problem_id, $plate_num, $brand, $model, $car_year, $connection);
      //selecting values from service table to be used in for each problem
      $price_time_temp = get_time_price($problems,$problem_id,$connection);
      $problem_time_temp = $price_time_temp[0];
      $service_price_temp = $price_time_temp[1];
      //converting time to int to be used in selectAllow
      $format_time = format_time($problem_time_temp);
      $problem_time_fl = $format_time[0];
      $problem_time_rounded = $format_time[1];
      $problem_time_temp1 = $format_time[2];
      //updating problems with missing data
      $sql_upd = "UPDATE problems SET car_id ='$car_id',problem_total_price= '$service_price_temp', problem_total_time ='$problem_time_temp1', worker_total_price='$service_price_temp' WHERE problem_id ='$problem_id' AND user_id ='$user_id' ";
      mysqli_query($connection, $sql_upd);
    }
  }
  else {
    echo $validator->get_readable_errors(true);
  }
  mysqli_close($connection);
}

?>
  <h3>Your data for selected problem is:</h3>
  <br>
    <p>Car plate number:
    <?php echo($plate_num);?></p>
    <p>Total price(changeable after admin review):
    <?php echo($service_price_temp);?></p>
    <p>Total time for selected problems(changeable after admin review):
    <?php echo( date('H:i:s', $problem_time_temp));?></p>
    <p>Your Comment:
    <?php echo($user_comment);?></p>
    <p>Please select the date and time range that works for you the best.<b>Time range is rounded on every half hour.</b></p> 
    <p>You can only select time range that not allready selected.</p>
  <!-- <br> Problemi:
  <?php print_r ($problems);?> -->
  <!-- Vreme trajanja zaokruzeno kao broj:
    <?php echo($problem_time_fl);?>
    <br> 
    Korisnikov ID u tabeli:
    <?php echo($user_id);?>
    <br>
    Vrme trajanja zaokruzeno u satima:
    <?php echo($problem_time_rounded);?> -->
  <br>
  <!-- DATE CAR YEAR:
    <?php echo($car_year);?>
    <br>
    PROBELEM ID:
    <?php echo($problem_id );?>
    <br>
    CAR ID:
    <?php echo($car_id );?> -->

  <script>
    $(document).ready(function () {
      $('#calendar').fullCalendar({
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        //set up duration to be = to time that is required for service
        snapDuration: '<?php echo($problem_time_rounded);?>',
        timeFormat: 'H:mm',
        minTime: "07:00:00",
        maxTime: "17:00:00",
        allDaySlot: false,
        height: 'auto',
        // adding header elements to Calendar
        customButtons: {
          myCustomButton: {
            text: 'Go Back to Main Menu',
            click: function () {
              window.location.href = "index.php?#pagehome.php";
            }
          }
        },

        header: {
          left: 'prev, next, today',
          center: 'myCustomButton title',
          right: 'month, agendaWeek,agendaDay, submit'
        },
        defaultView: 'agendaWeek',
        // upload data from MYSQL
        events: 'fullcalendar/load_reservation.php',
        // set up working hours and days
        businessHours: {
          start: '8:00',
          end: '16:00',
          dow: [1, 2, 3, 4, 5]
        },
        // doesn't give permision to overlap existing reservation
        selectOverlap: function (event) {
          return event.rendering === 'background';
        },

        // doesn't give permision to make reservation on non working days/hours
        selectConstraint: {
          start: '8:00',
          end: '16:00',
          dow: [1, 2, 3, 4, 5]
        },
        // prevents dates before today
        validRange: function (nowDate) {
          return {
            start: nowDate
          } // to prevent anterior dates
        },
        // only allow reservation that is = to 
        selectAllow: function (selectInfo) {
          var duration = moment.duration(selectInfo.end.diff(selectInfo.start));
          return duration.asHours() <= <?php echo($problem_time_fl);?>;
        },
        // option for selecting time interval for service
        
        selectable: true,
        selectHelper: true,
        unselectAuto: false,
        select: function (start, end) {

          var problem = <?php echo($problem_id);?>;
          if (problem) {

            var date = $.fullCalendar.formatDate(end, "Y-MM-DD");
            var start = $.fullCalendar.formatDate(start, "HH:mm:ss");
            var end = $.fullCalendar.formatDate(end, "HH:mm:ss");

            $.ajax({
              url: 'fullcalendar/upload_date.php',
              type: "POST",
              data: {
                problem: problem,
                date: date,
                start: start,
                end: end
              },
              dataType: 'JSON',
              cache: false,
              success: function (data) {
                swal(data);
              }
            })
          }
        },

      });
     
    });
  </script>
    <div id="calendar"></div>
    </div>
    </section>
    <!-- Footer -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="js/jquery.min.js"></script> -->
    <!-- <script src="js/popper.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
      crossorigin="anonymous"></script>
    <script src="js/tether.min.js"></script>
    <!-- <script src="js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
      crossorigin="anonymous"></script>
    <script src="js/index.js"></script>
    <!-- <script type="text/javascript" src="js/validation_form.js"></script>-->
</body>

</html>