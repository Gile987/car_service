<?php
/*Thhi is the step after user have enetered email for reseting password in password reset form, the link to this page
 is included from the forgot.php email message
*/
session_start();
require "include/config.php";
require "include/db.php";
require("include/gump.class.php");


// Make sure email and hash variables aren't empty
if (isset($_GET['email']) && !empty($_GET['email']) && isset($_GET['verification_code']) && !empty($_GET['verification_code'])) {

    $validator = new GUMP();

    $email = mysqli_real_escape_string($connection, $_GET['email']);
    $verification_code = mysqli_real_escape_string($connection, $_GET['verification_code']);

    $_POST = array(
        'email'	      => $email,
        'verificode'    => $verification_code
    );

    $_POST = $validator->sanitize($_POST);

    $rules = array(
        'email'	      => 'required|valid_email|min_len,3|max_len,32',
        'verificode'    => 'required|alpha_numeric'
    );

    $filters = array(
        'email'	      => 'trim|sanitize_string',
        'verificode'    => 'trim|sanitize_string'
    );

    $_POST = $validator->filter($_POST, $filters);


    $validated = $validator->validate(
        $_POST, $rules
    );


    var_dump($_POST);
    var_dump($validated);


    if($validated === TRUE) {

       
        // Make sure user email with matching hash exist
        $sql = "SELECT * FROM users WHERE email='$email' AND verification_code='$verification_code' AND verification_code !='' AND active='1'";

        $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {

                $verification_time = $row['verification_time'];
                // checking if 20 minutes for activation has expired
                if ($verification_time < date("Y-m-d H:i:s")) {
                    $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
                    <h4 class=\"alert-heading\">Error!</h4>
                    <p>Your verification code has expired</p>

                    <p class=\"mb-0\">Verification code is only active for 20 minutes, Plese reset your password again</p>

                    </div>";
                    header("location: index.php?#pagemessage.php"); 
                }
                    // if time for verification has not expired then:
                else {
                    $_SESSION['email'] = $email;
                    $_SESSION['verification_code'] = $verification_code;
                    header("location: index.php?#pageresetpassword.php");
                }
            }
        } elseif ($result->num_rows == 0) {
            $_SESSION['message'] = "<h1>Error</h1><br><p>You have entered invalid URL for password reset!<p>";
            header("location: index.php?#pagemessage.php");
        }
    }
    else{
        print_r($validated); // Shows all the rules that failed along with the data
    }       


} 
else {
    header("location: index.php");
}
