<?php

session_start();
require("include/config.php");
require("include/db.php");
require("include/gump.class.php");

if (!isset($_POST['login'])) {
    header("location: index.php");
}
else {

    $validator = new GUMP();

    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);

    $_POST = array(
        'email'   => $email,
        'password'=> $password
    );

    $_POST = $validator->sanitize($_POST);

    $rules = array(
        'email'   => 'required|valid_email|min_len,3|max_len,32',
        'password'=> 'required|min_len,6|max_len,20'
    );

    $filters = array(
        'email'   => 'trim|sanitize_email',
        'password'=> 'trim|sanitize_string'
    );

    $_POST = $validator->filter($_POST, $filters);


    $validated = $validator->validate(
        $_POST, $rules
    );

    if($validated === TRUE) {
        
        $password_temp = SALT1."$password".SALT2;
        $password_hash = MD5($password_temp);

        $sql = "SELECT user_id, role_id FROM users
            WHERE email = '$email'
            AND password = '$password_hash' AND active=1";
        $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));


        if (mysqli_num_rows($result)>0) {
            while ($record = mysqli_fetch_array($result,MYSQLI_BOTH)) {
                $_SESSION['user_id'] = $record['user_id'];
                $_SESSION['role_id'] = $record['role_id'];  
            }
            // notification of new problems if admin is logged in
            if ($_SESSION['role_id'] == 1) {
                $sql =  "SELECT * FROM problems WHERE problem_status_id='1'";
                $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

                if (mysqli_num_rows($result)>0) {
                    $row_cnt = $result->num_rows;
                }
                $_SESSION['new_problems'] = $row_cnt;
            }
            $name = $_SESSION['name'];
            $_SESSION['message'] ="<div class=\"alert alert-success\" role=\"alert\">
            <h4 class=\"alert-heading\">Success!</h4>
            <p>You have successfully logged into the Car Mechanic services! </p>

            <p class=\"mb-0\">Welcome $name.</p>

            </div>";

            header("location: index.php?#pagemessage.php");
        }
        
        else {
            // User doesn't exist or wrong password
            $_SESSION['message'] ="<div class=\"alert alert-warning\" role=\"alert\">
            <h4 class=\"alert-heading\">Error!</h4>
            <p>User with this name does not exist in our DB or you need to reset your password.</p>

            <p class=\"mb-0\">Please reset your password if you are already registered. If you are not, then register</p>

            </div>";
        
            header("location: index.php?#pagemessage.php");
        }
    }
    else{
        echo $validator->get_readable_errors(true);
    }

}
?>