<?php
session_start();
require("include/config.php");
require("include/db.php");
require("include/functions.php");
require("include/gump.class.php");

// Make sure the form is being submitted with method="post"
if (!isset($_POST['firstname'])) {
    header("location: index.php");
}
else{

    $validator = new GUMP();

    $first_name = mysqli_real_escape_string($connection, $_POST['firstname']);
    $last_name = mysqli_real_escape_string($connection, $_POST['lastname']);
    $email_from = mysqli_real_escape_string($connection, $_POST['email']);
    $comment = mysqli_real_escape_string($connection, $_POST['contact_text']);

    $_POST = array(
        'firstname'   => $first_name,
        'lastname' 	  => $last_name,
        'emailfrom'	  => $email_from,
        'comment'     => $comment
    );
    
    $_POST = $validator->sanitize($_POST);


    $rules = array(
        'firstname'   => 'required|alpha|min_len,3|max_len,15',
        'lastname' 	  => 'required|alpha|min_len,3|max_len,15',
        'emailfrom'	  => 'required|valid_email',
        'comment'     => 'required|max_len,1000'
    );

    $filters = array(
        'firstname'   => 'trim|sanitize_string',
        'lastname' 	  => 'trim|sanitize_string',
        'emailfrom'	  => 'trim|sanitize_email',
        'comment'     => 'trim|sanitize_string'
    );

    $_POST = $validator->filter($_POST, $filters);

    $validated = $validator->validate(
        $_POST, $rules
    );

    if($validated === TRUE) {
        $email = "car.service@mladenreljic.com";
        $name ="car service";
        $message = "Name: <p>$first_name</p>
        Last Name: <p>$last_name</p>
        Email: <p>$email_from</p>
        Comment: <p>$comment</p>
        ";
    
        $response = sendEmail($email, $name, $message);
    
        if ($response == 1) {
           
            $_SESSION['message'] = "<div class=\"alert alert-success\" role=\"alert\">
            <h4 class=\"alert-heading\">Success!</h4>
            <p class=\"mb-0\">Thank you for contacting us!</p>
    
            </div>";
            header("location: index.php?#pagemessage.php");
        }
    
        else {
            $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
            <h4 class=\"alert-heading\">Error!</h4>
            <p>Sorry, something went wrong. Please try again later..</p>
    
            </div>";
            header("location: index.php?#pagemessage.php");
        }
    }
    else{
        echo $validator->get_readable_errors(true);
    } 
}



?>