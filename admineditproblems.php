<?php
session_start();
$problem_status_id=$_SESSION['problem_status_id'];

require("include/config.php");
require("include/db.php");
require("include/functions.php");

if(!isset($_POST['problem_id']) && !isset($_POST['user_email']) && !isset($_POST['user_name'])){

    header("location: index.php");
}
else

{


$problem_id = mysqli_real_escape_string($connection,$_POST['problem_id']);
$user_email = mysqli_real_escape_string($connection,$_POST['user_email']);
$user_name = mysqli_real_escape_string($connection, $_POST['user_name']);
$problem_total_price = mysqli_real_escape_string($connection, $_POST['problem_total_price']);
$reservation_date = mysqli_real_escape_string($connection, $_POST['reservation_date']);
$reservation_start = mysqli_real_escape_string($connection, $_POST['reservation_start']);
$reservation_end = mysqli_real_escape_string($connection, $_POST['reservation_end']);
$admin_comment = mysqli_real_escape_string($connection, $_POST['admin_comment']);
$user_comment = mysqli_real_escape_string($connection, $_POST['admin_price']);
$admin_price = mysqli_real_escape_string($connection, $_POST['admin_price']);
$problem_status = mysqli_real_escape_string($connection, $_POST['status']);


//sub totla price +added/discounted worker price
$worker_total_price = $problem_total_price + $admin_price;

$sql = "SELECT problem_status_id FROM problem_status WHERE status='$problem_status'";

    $result = mysqli_query($connection,$sql) or die(mysql_error($connection));
//if yes, that check if its in service or its done
while($row = $result->fetch_assoc()) {
        $status = $row['problem_status_id'];    
}

//first update inserted data from input fields
$sql_upd = "UPDATE problems SET problem_status_id='$status', problem_status_update=NOW(), worker_comment='$admin_comment',worker_added_price='$admin_price',  worker_total_price='$worker_total_price' WHERE problem_id='$problem_id'";

if ($connection->query($sql_upd) === TRUE) {
    $msg = "You have successfully saved a problem.";
} else {
    $msg = "Error saving record: " . $connection->error;
}
//DONE with inserting data

$sql = "SELECT problem_service.service_id,service_name, problem_service_time FROM problem_service INNER JOIN services ON problem_service.service_id = services.service_id WHERE problem_id='$problem_id'";

$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));
if ($result->num_rows > 0) {
$problem_time_temp = "00:00:00";
$problem_time_temp = strtotime($problem_time_temp);
$services =array();
while ($row = $result->fetch_assoc()) {
  $services['service'][] =array('service_id'=>$row['service_id'],'service_name'=>$row['service_name'],'problem_service_time'=>$row['problem_service_time']);
    $service_time = $row['problem_service_time']; 
    $problem_time_temp = $problem_time_temp + strtotime($service_time) - strtotime("00:00:00");
}
}
//converting time to int to be used in selectAllow With function
$format_time = format_time($problem_time_temp);
$problem_time_fl = $format_time[0];
$problem_time_rounded = $format_time[1];
$problem_time_temp1 = $format_time[2];
$services =json_encode($services);

}

?>
  <!doctype html>
  <html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
   
    <link href="fullcalendar/lib/fullcalendar.min.css" rel="stylesheet" />
    <link href="fullcalendar/scheduler.min.css" rel="stylesheet" />
    <script src="fullcalendar/lib/jquery.min.js"></script>
    <script src="fullcalendar/lib/moment.min.js"></script>
    <script src="fullcalendar/lib/fullcalendar.min.js"></script>
    <script src="fullcalendar/scheduler.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7/dist/sweetalert2.all.min.js"></script>
    <title>Car Workshop</title>
  </head>

  <body>
    <nav class="navbar navbar-expand-md navbar-light fixed-top justify-content-center">
      <div class="container" id="navi">
        <a class="navbar-brand em-text" href="index.php">
          <img src="img/logo.png" width="30" height="30" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="#navbarsExample09"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample09">


          <ul class="nav navbar-nav navbar-right flex-row justify-content-start ml-auto">
            <?php
          if(isset($_SESSION['user_id'])) {
            $name = get_user_name($_SESSION['user_id']);
              
            echo "<li><a href=\"logout.php\" class=\"nav-link\">Logout</a></li>";
              		          
        }                               
          ?>
          </ul>
        </div>
      </div>
    </nav>

    <?php	

  if(isset($_SESSION['user_id']) && isset($_SESSION['role_id']) && $_SESSION['role_id'] == 1) {//ukoliko je role_id 1 ima privilegije da insertuje novi produsct
    
  echo <<<EOT
  
  <nav class="navbar fixed-top navbar-light navbar-left bg-light justify-content-center" id="navi2">
    <div class="container">
      <ul class="nav  mr-auto">
        <li>
          <p class="navbar-text navbar-left"><strong>Welcome $name</strong></p>
        </li>
      </ul>
      
     </div>
  </nav>

EOT;
  }
  
  ?>

      <section id="middle">
        <div class="container" id="pageContent">
          <br>
          <h3>Your data for selected problem is:</h3>
          <br>
          <p>Total time for problem #
            <?php echo($problem_id);?>to complete all services is:
            <?php echo( date('H:i:s', $problem_time_temp));?>.</br>
            <b>Time range is rounded on every half hour.</b>
          </p>
          <p>Changing the duration of each service time will efect the scheduled date/time</p>
          <p>You can manualy pick new scheduled date/time if total service time is longer than reservation time!</p>

          <form class="formvalidate" >
            <div class="form-group">
              <?php
                            $date_now = (new \DateTime())->format('Y-m-d');
                            $arr = json_decode($services,true);
                            
                            echo "<p>Services for problem #$problem_id are:</p>";
                           foreach($arr['service'] as $keys=>$value){
                          echo "<label>".$value['service_name']."</label><br><input  class=\"service_time\" name=".$value['service_id']." type=\"text\" value=".$value['problem_service_time']."><br>";
                          
                        }  
                            echo "<p>Service is scheduled for(marked with green):</p>
                            <label>Scheduled Date</label><br><input type=\"date\" name=\"reservation_date\"
                            value=".$reservation_date." min=".$date_now."><br><label>Scheduled Start Time</label><br><input type=\"text\" name=\"reservation_start\" placeholder=\"HH:MM:SS\" pattern=\"([0-1]{1}[0-9]{1}|20|21|22|23):[0-5]{1}[0-9]{1}:[0-5]{1}[0-9]{1}\" value=".$reservation_start."><br><label>Scheduled End Time</label><br><input type=\"text\" name=\"reservation_end\" placeholder=\"HH:MM:SS\" pattern=\"([0-1]{1}[0-9]{1}|20|21|22|23):[0-5]{1}[0-9]{1}:[0-5]{1}[0-9]{1}\" value=".$reservation_end."><br>";
                            ?>
            </div>
            <button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>
          </form>
          <br>

          <div id="calendar"></div>
        </div>
      </section>
      
      <script>

        $(document).ready(function () {
          $('#calendar').fullCalendar({
            schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
            // defaultView: 'agendaDay',
            //set up duration to be = to time that is required for service
            snapDuration: '<?php echo($problem_time_rounded);?>',
            timeFormat: 'H:mm',
            minTime: "07:00:00",
            maxTime: "17:00:00",
            allDaySlot: false,
            height: 'auto',
            //adding header e
            //adding header elemnts to Calendar
            header: {
              left: 'prev, next, today',
              center: 'myCustomButton title',
              right: 'month, agendaWeek, submit'
            },
            defaultView: 'agendaWeek',
            //upload data from MYSQL
            events: 'fullcalendar/load_reservation.php',
            editable: false,
            eventRender: function (event, element) {
              if (event.title == "<?php echo($problem_id);?>") {
                element.css('background-color', '#0f0');
                event.editable = true;
              }
            },

            //set up working hours and days
            businessHours: {

              start: '8:00',
              end: '16:00',
              dow: [1, 2, 3, 4, 5]
            },
            //dosn't give permision to overlap existing reservation
            selectOverlap: function (event) {
              return event.rendering === 'background';
            },

            //dosn't give permision to make reservation on non working days/hours
            selectConstraint: {
              start: '8:00',
              end: '16:00',
              dow: [1, 2, 3, 4, 5]
            },
            //prevents dates before today
            validRange: function (nowDate) {
              return {
                start: nowDate
              } //to prevent anterior dates
            },

          });
          // to start on specific date
          $('#calendar').fullCalendar('gotoDate', '<?php echo($reservation_date);?>');


          $('#btn-submit').on('click', function (e) {
            e.preventDefault();
            var formdata = $(this).parents('form').find('input').serializeArray();
            var data_input = {};
            //converting to JSON Object
            $(formdata).each(function (index, obj) {
              data_input[obj.name] = obj.value;
            });
            // console.log(data_input)
            //comparing if total time of sevices is longer than reservation time using moment.js 
            var start = moment(data_input.reservation_start, "HH:mm:ss");
            var end = moment(data_input.reservation_end, "HH:mm:ss");
            reservation_start = moment(start).format("HH:mm:ss");
            reservation_end = moment(end).format("HH:mm:ss");
            start_ashour=moment.duration(reservation_start).asHours();
            end_ashour=moment.duration(reservation_end).asHours();
           
            reservation_hours = end_ashour - start_ashour;
            
            var service_time = [];
            var problems = {};
            $('.service_time').each(function (index, obj) {
              problems[this.name] = this.value;
              service_time.push(moment.duration(this.value).asHours());
            });
            
            //use values after the loop
            var service_total_hours = 0;
            $.each(service_time, function () {
              service_total_hours += this;
            });
            //round
            service_total_hours = Math.round((service_total_hours * 2) + 0.49999) / 2;
        
            if (reservation_hours < service_total_hours) {
              swal({
                title: 'Change the Scheduled date/time',
                text: 'Total Service time is longer than Reservation time',
                type: 'warning',
                footer: 'Total Service time:'+service_total_hours+'h and Reservatin time:'+reservation_hours+'h',
              });
            } 
            else if(reservation_start<"08:00:00" || reservation_end>"16:00:00" || reservation_hours > service_total_hours)
             {
              swal({
                title: "Change the Scheduled date/time",
                text: "Reservation time is out of working hours or its longer than Total Service Time",
                type: "warning",
                input: 'radio',
                inputOptions: {
                  'send': 'Send Email to User and Save',
                  'save': 'Just Save',
                },
                inputValue: 'save',
                showCancelButton: true,
                confirmButtonText: "Yes, send it!",
                cancelButtonText: "No, cancel pls!",
              }).then((result) => {
                if (result.value) {

                  $.ajax({
                    type: "post",
                    url: "adminsavetimeproblems.php",
                    data: {
                      mail_opt: result.value,
                      problem_id: '<?php echo($problem_id);?>',
                      user_email: '<?php echo($user_email);?>',
                      user_name: '<?php echo($user_name);?>',
                      problem_total_price: '<?php echo($problem_total_price);?>',
                      status: '<?php echo($problem_status);?>',
                      admin_comment: '<?php echo($admin_comment);?>',
                      admin_price: '<?php echo($admin_price);?>',
                      problems: problems,
                      reservation_date: data_input.reservation_date,
                      reservation_start: data_input.reservation_start,
                      reservation_end: data_input.reservation_end,
                    },
                    dataType: 'html',
                    cache: false,
                    success: function (data) {
                      console.log(4);
                      swal(data);
                      setTimeout(
                        "window.location='admin/jsonfordatatable.php?problem_status_id=<?php echo ($problem_status_id);?>'",
                        3000);
                    },
                    //http://api.jquery.com/jQuery.ajax/
                    error: function (jqXHR, textStatus, errorThrown) {
                      if (jqXHR.status == 500) {
                        alert('Internal Server Error: ' + jqXHR.responseText);
                      } else if (jqXHR.status == 404) {
                        alert('Requested page not found: ' + jqXHR.responseText);
                      } else {
                        alert('Unexpected error.');
                      }
                    }

                  });
                }
              });

              return false;
            }
            else{
              swal({
                title: 'Select Option',
                input: 'radio',
                inputOptions: {
                  'send': 'Send Email to User and Save',
                  'save': 'Just Save',
                },
                inputValue: 'save',
                showCancelButton: true
              }).then((result) => {
                if (result.value) {
                  $.ajax({
                    type: "post",
                    url: "adminsavetimeproblems.php",
                    data: {
                      mail_opt: result.value,
                      problem_id: '<?php echo($problem_id);?>',
                      user_email: '<?php echo($user_email);?>',
                      user_name: '<?php echo($user_name);?>',
                      problem_total_price: '<?php echo($problem_total_price);?>',
                      status: '<?php echo($problem_status);?>',
                      admin_comment: '<?php echo($admin_comment);?>',
                      admin_price: '<?php echo($admin_price);?>',
                      problems: problems,
                      reservation_date: data_input.reservation_date,
                      reservation_start: data_input.reservation_start,
                      reservation_end: data_input.reservation_end,
                    },
                    dataType: 'html',
                    cache: false,
                    success: function (data) {
                      console.log(4);
                      swal(data);
                      setTimeout(
                        "window.location='admin/jsonfordatatable.php?problem_status_id=<?php echo ($problem_status_id);?>'",
                        3000);

                    },
                    //http://api.jquery.com/jQuery.ajax/
                    error: function (jqXHR, textStatus, errorThrown) {
                      if (jqXHR.status == 500) {
                        alert('Internal Server Error: ' + jqXHR.responseText);
                      } else if (jqXHR.status == 404) {
                        alert('Requested page not found: ' + jqXHR.responseText);
                      } else {
                        alert('Unexpected error.');
                      }
                    }

                  });
                }

              })

            }
            
});
        });
      </script>
       