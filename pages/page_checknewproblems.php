<?php
session_start();

unset($_SESSION['new_problems']);
$problem_status_id=$_SESSION['problem_status_id'];
// This page is visible only to the admin (role_id = 2)
if (!isset($_SESSION['user_id']) && !isset($_SESSION['role_id']) && $_SESSION['role_id'] !== 1) {
    $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
  <h4 class=\"alert-heading\">Error!</h4>

  <p class=\"mb-0\">You do not have the permission to view this page!</p>

  </div>";
    header("location: page_message.php");
    exit();
}
?>
 
  <table id="myTable" class="stripe row-border order-column" style="width:100%">
    <thead>
      <tr>
        <th>More details</th>
        <th>ID</th>
        <th>User Name</th>
        <!-- <th>User phone</th>
        <th>User email</th> -->
        <th>Car plate</th>
        <!-- <th>Car Brand</th>  -->
        <!-- <th>Car Model</th>
        <th>Car year of production</th> -->
        <th>Problem Date</th>
        <!-- <th>Problem status updated</th> -->
        <th>User comment</th>
        <th>Admin comment</th>
        <!-- <th>Problem Total Price</th> -->
        <th>Added/Discount price</th>
        <!-- <th>Problem Total Time</th> -->
        <th>Final Price</th>
        <!-- <th>Problem Date finished</th>  -->
        <th>Problem Status</th>
        <th>Admin changes</th>
      </tr>
    </thead>
    <tfoot>
    <tr>
        <th>More details</th>
        <th>ID</th>
        <th>User Name</th>
        <th>Car plate</th>
        <th>Problem Date</th>
        <th>User comment</th>
        <th>Admin comment</th>
        <th>Added/Discount price</th>
        <th>Final Price</th>
        <th>Problem Status</th>
        <th>Admin changes</th>
      </tr>
  </table>


    <script>
        /* Formatting function for row details - modify as you need */
        function format(d) {
            // `d` is the original data object for the row
            if(d.problem_finished==null){
                d.problem_finished='Still not finished.';
                }
                if(d.problem_status_update==null){
                    d.problem_status_update='Problem received, waiting for update.';
                }
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>User phone:</td>' +
                '<td>' + d.phone + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>User email:</td>' +
                '<td>' + d.pemail + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Car Brand:</td>' +
                '<td>' + d.car_brand + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Car Model:</td>' +
                '<td>' + d.car_model + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Car year of production:</td>' +
                '<td>' + d.car_year + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Problem status last update:</td>' +
                '<td>' + d.problem_status_update + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Problem Total Price:</td>' +
                '<td>' + d.problem_total_price + '</td>' +
                '</tr>' +

                '<tr>' +
                '<td>Problem Total Time:</td>' +
                '<td>' + d.problem_total_time + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Problem reservation date:</td>' +
                '<td>' + d.reservation_date + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Problem reservation start time:</td>' +
                '<td>' + d.reservation_start + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Problem reservation end time:</td>' +
                '<td>' + d.reservation_end + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Problem Date finished:</td>' +
                '<td>' + d.problem_finished + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extra info:</td>' +
                '<td>For additional info about each service please select EDIT optin from the table row' +
                '</tr>' +
                '</table>';
        }
        $("div.toolbar")
                //         .html(
                //             '<b>All problems Table</b></br><button name="reload" style="font-size:16px">Reload<i class="fa fa-refresh fa-spin"></i>'
                //         );
                // },

        $(document).ready(function () {
            $('#myTable tfoot th').not(":eq(0),:eq(1),:eq(7),:eq(9),:eq(10)") //Exclude columns 3, 4, and 5
                .each( function () {
               var title = $('#myTable thead th').eq( $(this).index() ).text();
                $(this).html( '<input style="width:125px;" type="text" placeholder="Search '+title+'" />' );
                } );

            var table = $('#myTable').DataTable({
               dom: 'fi<"toolbar">rt<"bottom"lp><"clear">',
                
                initComplete: function(){
                $("div.toolbar").html('<button type"button" id="btn_rld" style="font-size:16px">Reload<i class="fa fa-refresh fa-spin"></i>'); 
                },
                scrollX: true,
                scrollCollapse: true,
                responsive: true,
                select: true,
                ajax: {
                    url: 'json/problems_data.json',
                    dataSrc: 'rows'
                },
                columns: [{
                        className: 'details-control',
                        orderable: false,
                        data: null,
                        defaultContent: '<i class="fa fa-plus-square" aria-hidden="true"></i>',
                        width: "20px"
                    },
                    {
                        data: 'problem_id',
                        width: "20px",
                        className: "text-center",
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).attr('value', cellData);
                        }
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'plate_num',
                        width: "15px",
                    },
                    {
                        data: 'problem_date',
                    },
                    {
                        data: 'user_comment',
                    },
                    {
                        data: 'worker_comment',
                        render: function (data, type) {
                            if(data==null){
                                data='';
                            }
                            return '<textarea name="worker_commente" rows="4" cols="15" value="' +
                                data + '"> '+
                                data +'</textarea>';
                        },
                    },
                    {
                        data: 'worker_added_price',
                        className: "text-center",
                        render: function (data, type) {
                            return '<input type="number" name="worker_added_price" step="0.01" min="-10000" max="100000" style="width:100px" value="' +
                                data + '"/>';
                        },
                    },
                    {
                        data: 'worker_total_price',
                        className: "text-center",
                    },
                    {
                        data: 'status',
                        width: "15px",
                        render: function (data, type, full, meta) {
                            chckbohx =
                                '<select type="text" id="status" name="status" style="width:100px" required="required"> <option selected value="' +
                                $('<div/>').text(data).html() + '">' + $('<div/>').text(data).html() +
                                '</option>';
                            $.each(full.problem_status, function (key, value) {
                                if (value != data) {
                                    chckbohx = chckbohx + '<option value="' + $(
                                        '<div/>').text(value).html() + '">' + $(
                                        '<div/>').text(value).html() + '</option>';
                                }
                            });
                            return chckbohx;
                        },
                    },
                    {
                        data: null,
                        defaultContent: '<div><button class="btn-save" style="font-size:12px; color:green">SAVE&nbsp<i class="fa fa-save"></i></button><br><br><button class="btn-edit" style="font-size:12px; color:blue">EDIT TIME<!--<i class="fa fa-edit"></i>--></button><br><br><button class="btn-del" style="font-size:12px; color:red">&nbspDELETE&nbsp<i class="fa fa-trash-o"></i></button></div>',
                        width: "80px",
                    }

                ],
                order: [
                    [1, 'asc']
                ]

            });
            $('.toolbar').on('click', '#btn_rld', function (e) {
                e.preventDefault();
                location.href = "admin/jsonfordatatable.php?problem_status_id=<?php echo ($problem_status_id);?>";
                
            });


            // Add event listener for opening and closing details
            $('#myTable tbody').on('click', 'td.details-control', function () {
                var tr = $(this).parents('tr');
                var row = table.row(tr);
                var tdi = tr.find("i.fa");

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    tdi.first().removeClass('fa-minus-square');
                    tdi.first().addClass('fa-plus-square');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                    tdi.first().removeClass('fa-plus-square');
                    tdi.first().addClass('fa-minus-square');
                }
            });
//EDIT from Table
$('#myTable tbody').on('click', 'button.btn-edit', function (e) {
                e.preventDefault();
                //selecting data from table that is not new inserted
                var data = table.row($(this).parents('tr')).data();
                //selecting new inputed data from row
                var formdata = $(this).parents('tr').find('input, select, textarea').serializeArray();
                var data_input = {};
                //converting to JSON Object
                $(formdata).each(function (index, obj) {
                    data_input[obj.name] = obj.value;
                });
                //combining new inserted data from table with old data
               var dataJSON =  {
                                problem_id: data.problem_id,
                                user_email: data.pemail,
                                user_name: data.name,
                                reservation_date: data.reservation_date,
                                reservation_start: data.reservation_start,
                                reservation_end: data.reservation_end,
                                problem_total_price: data.problem_total_price,
                                status: data_input.status,
                                admin_comment:data_input.worker_commente,
                                admin_price:data_input.worker_added_price};
            //using POST metod to send JSON to php page
            $.redirect('admineditproblems.php', dataJSON);
            //using GET metod to send JSON to php page
            //location.href = 'admineditproblems?data='+JSON.stringify(dataJSON);
            });
            //Save from Table
            $('#myTable tbody').on('click', 'button.btn-save', function (e) {
                e.preventDefault();
                //selecting data from table that is not new inserted
                var data = table.row($(this).parents('tr')).data();
                //selecting new inputed data from row
                var formdata = $(this).parents('tr').find('input, select, textarea').serializeArray();
                var data_input = {};
                //converting to JSON Object
                $(formdata).each(function (index, obj) {
                    data_input[obj.name] = obj.value;
                });
                swal({
                    title: 'Select Option',
                    input: 'radio',
                    inputOptions: {
                        'send': 'Send Email to User and Save',
                        'save': 'Just Save',
                    },
                    inputValue: 'save',
                    showCancelButton: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "adminsaveproblems.php",
                            data: {
                                mail_opt: result.value,
                                problem_id: data.problem_id,
                                user_email: data.pemail,
                                user_name: data.name,
                                problem_total_price: data.problem_total_price,
                                status: data_input.status,
                                admin_comment:data_input.worker_commente,
                                admin_price:data_input.worker_added_price,

                            },
                            dataType: 'html',
                            cache: false,
                            success: function (data) {
                                console.log(4);
                               swal(data);

                            },
                            //http://api.jquery.com/jQuery.ajax/
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (jqXHR.status == 500) {
                                    alert('Internal Server Error: ' + jqXHR.responseText);
                                } else if (jqXHR.status == 404) {
                                    alert('Requested page not found: ' + jqXHR.responseText);
                                } else {
                                    alert('Unexpected error.');
                                }
                            }

                        });
                    }

                })

            });
            //Delete from table
            $('#myTable tbody').on('click', 'button.btn-del', function (e) {
                e.preventDefault();
                //selecting data from table that is not new inserted
                var data = table.row($(this).parents('tr')).data();
                console.log(data);

                swal({
                    title: 'Select Option',
                    input: 'radio',
                    inputOptions: {
                        'send': 'Send Email to User and DELETE',
                        'save': 'Just DELETE',
                    },
                    inputValue: 'save',
                    showCancelButton: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            url: "admindeleteproblems.php",
                            data: {
                                mail_opt: result.value,
                                problem_id: data.problem_id,
                                user_email: data.pemail,
                                user_name: data.name
                            },
                            dataType: 'html',
                            cache: false,
                            success: function (data) {
                                console.log(4);
                                swal(data);

                            },
                            //http://api.jquery.com/jQuery.ajax/
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (jqXHR.status == 500) {
                                    alert('Internal Server Error: ' + jqXHR.responseText);
                                } else if (jqXHR.status == 404) {
                                    alert('Requested page not found: ' + jqXHR.responseText);
                                } else {
                                    alert('Unexpected error.');
                                }
                            }

                        });
                    }
                })

            });

            // Keeps the expander from being selected
            table.on("user-select", function (e, dt, type, cell, originalEvent) {
                if ($(cell.node()).hasClass("details-control")) {
                    e.preventDefault();
                }
            });


// Apply the search
table.columns().eq( 0 ).each( function ( colIdx ) {
        if (colIdx == 0 || colIdx == 1 || colIdx == 7 || colIdx == 9 || colIdx == 10) return; //Do not add event handlers for these columns

        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );


        });
    </script>

