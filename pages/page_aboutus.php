<?php
    echo <<<EOT

    <div class="row">
      <div class="col-lg-8">
        <h2>About Us</h2>
        <p>We are a team of hard-working car mechanics with over 50 years of combined experience. We love cars, and our mission is to 
        provide you with the best service possible.
        </p>
        <br>

        <div id="accordion" role="tablist">
          <div class="card">
            <div class="card-header" role"tab" id="headingOne">
              <h6 class="mb-0">
                <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls"collapseOne">Experience</a>
              </h6>
            </div>
            <a id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                Ever since we have learned how to walk, we have been interested in the intricacies and inner workings of cars. We started off as any other vehicle enthusiast, by
                enjoying the aesthetics of cars, but soon enough we wanted to learn more. Our first steps into the world of car repair were together with out fathers,
                and soon enough we were capable of fixing simple problems, but our thirst for knowledge couldn't be quenched. As we grew older, so did our appetite, but 
                we had a goal set in front of us - learn everything there is to learn about cars by working on them. Some of us worked for other car mechanics, while others
                tried setting up their own car workshops. Soon enough, we got together by pure chance, and decided to combine our knowledge by opening up this car workshop.
                We have plenty of experience, which hundreds of satisfied customers are sure to vouch for. We hope that our experience will show when you come to pick up 
                your fixed vehicle, and we hope that you will be leaving our workshop with a smile.
              </div>
            </a>
          </div>

          <div class="card">
            <div class="card-header" role"tab" id="headingTwo">
              <h6 class="mb-0">
                <a data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls"collapseOne">Customer is King</a>
              </h6>
            </div>
            <a id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                We live by the motto "customer is king," which is why we are always looking forward to hear more from you. If you have any suggestions to make us better, we 
                will listen to you. After all, our goal is to be your number one source for car repairs. On the off-chance that you have any complaints, we will make sure 
                that you are heard out, and we will try our best to rectify the situation. When you are in our workshop, you can be sure that we will treat you as a dearest 
                guest, and that you will be accomodated. Our waiting rooms are pleasant and relaxing, with plenty of reading material. If you get thirsty, our friendly 
                staff will make sure to meet your needs - that is a promise! We can't wait to meet you, and we hope that you will find out why so many people keep coming 
                back for our services.
              </div>
            </a>
          </div>

          <div class="card">
            <div class="card-header" role"tab" id="headingThree">
              <h6 class="mb-0">
                <a data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls"collapseOne">High-Quality Service</a>
              </h6>
            </div>
            <a id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                We strive to provide you with the best service, and to do that we provide you with high-quality car parts only. Besides the unparalleled quality of car parts, 
                we work hard to train our mechanics in doing excellent work. All of our mechanics pass an intense training course, which teaches them to strive for perfection. 
                All of our customers get a preferential treatment, and we hope that you too will leave our workshop with a smile on your face.
              </div>
            </a>
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <img src="img/demo1.png" class="car-photo img-fluid">
        <h2>Learn More</h2>
        <p>If you are curious to learn more about our car workshop, or if you have any questions, don't be a stranger and contact us. We will do our best to answer 
        all of your questions and provide you with the necessary information.
        </p>
        <a href="#pagecontact.php" class="btn btn-outline-secondary" role="button">Contact Us</a>
      </div>
    </div>
EOT;

?>