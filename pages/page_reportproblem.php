<?php
session_start();
require("../include/config.php");
require("../include/db.php");    
//only if user is loged in this page will return data to page check new problems
if (!isset($_SESSION['user_id']) && !isset($_SESSION['role_id']) && $_SESSION['role_id'] !== 2) 
{
    $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
  <h4 class=\"alert-heading\">Error!</h4>

  <p class=\"mb-0\">You do not have the permission to view this page!</p>

  </div>";
    header("location: page_message.php");
    exit();
}
?>
<form class="formvalidate" action="insertdate.php" method="POST" enctype="multipart/form-data">
<fieldset>
<label for="plate_num">Vehicle Registration Number (only letters, numbers, and spaces)</label></br>
<input type="text" name="plate_num" id="plate_num"></br></br>
<label for="brand">Car Brand</label></br>
<select name="brand" id="brand">
<option value="" selected="Selected">Choose your car brand</option>
</select>
</br></br>
<label for="model">Car model</label></br>
<select name="model" id="model">
<option value="">Choose your car model</option>
</select>
</br></br>
<label for="car_year" id="car_year">Car Year</label></br>
<?php
echo '<select name="car_year">';
echo '<option value="" selected="Selected">Choose a Year</option>';
for ($i=1950; $i<=date("Y"); $i++) { 
    echo '<option value="'.$i.'">'.$i.'</option>';
}
echo '</select>';
?>
</br></br>
<label for="problems[]">Problem</label></br>
<select class="required" name="problems[]" id="problems" multiple>
<?php
echo "<option value=\"\" disabled>Choose your problem</option>";
$sql = "SELECT * FROM services ORDER BY service_time";

    $result = mysqli_query($connection,$sql) or die(mysql_error($connection));

    if (mysqli_num_rows($result)>0) {
    while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
        echo "<option value=\"$record[service_id]\">$record[service_name]</option>";
    }
 ?>
</select>
</br></br>
<label for="user_comment">Message</label>
</br>
<textarea id="user_comment" name="user_comment" placeholder="Your Message"></textarea> 
</fieldset>
</br>
<button type="submit" class="btn btn-outline-secondary" name="insertproblem" id="insertproblem">Submit</button>
</form>
<script>
$(document).ready(function() {
    //geting from JSON data for brands
    $.getJSON("json/brand_models.json", function(data) {
        $.each(data, function(key, value) {
            $("#brand").append("<option value='"+value.value+"'>" + value.title + "</option>");
        });
    });
    //selecting brand of car-depending from brand selecting models
    $('#brand').change(function() {
        //value of brand in JSON will be used for selecting the one brand
        var $cid=$('#brand').val();
        //selecting title from chosen brand
        var $cid_title = this.options[this.selectedIndex].text;
        //reseting model selection
        $('#model').empty();
        $('#model').append('<option value="" selected="Selected" disabled >Odaberite model za marku '+$cid_title+' </option>');
        //comparing if selected brand is differenth than OST
        if($cid !='OST'){
            //selecting models from JSON
            $.getJSON("json/brand_models.json", function(json) {
                $.each(json, function (key, data1) {
                // checking for models in data1 that are == with selected brand
                    if(data1.value === $cid){
                        data1.models.forEach(function (models) {
                            $("#model").append("<option value='"+models.value+"'>" + models.title + "</option>");
                        });
                    }
                });
            });
        }    
        else {
            //if selected brand is OST than the model is only other
            $('#model').empty();
            $('#model').append('<option value="other" selected="Selected">Ostalo </option>');
        }   
    });
});

var url = "js/validation_form.js";
$.getScript(url);

</script>
