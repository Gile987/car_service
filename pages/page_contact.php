<?php

  echo <<<EOT
    <div class="row">
      <div class="col-lg-8">
        <form class="formvalidate" action="contactmail.php" method="post">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="firstname" class="form-control">
          </div>
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="lastname" class="form-control">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control">
          </div>
          <div class="form-group">
            <label>Message</label>
            <textarea name="contact_text" class="form-control" placeholder="Your Message"></textarea>                    
          </div>
          <button type="submit" name="contactus" class="btn">Submit</button>  
        </form>
      </div>
      </br>
      </br>
      <div class="col-lg-4">
        <div class="container-fluid">
          <div class="map-responsive">
            <div id="map"></div>
          </div>
        </div>
      </div>
    </div>
  
EOT;
   
?>
<script>
var url = "js/validation_form.js";
$.getScript(url);

function initMap() {
  var mapStyle = '';
  $.get( 'js/map.js', function( data ) {
    if (data) {
    mapStyle = JSON.parse(data);
    map_options = {
      zoom: 16,
      center: {lat: 46.09950038963794, lng: 19.668612996434046},
      styles: mapStyle
    }

    map_document = document.getElementById('map')
    map = new google.maps.Map(map_document,map_options);
    }
  });
}
</script>
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3WkwLk4bSEJMwF2FfOUVrhqztFcHmvoA&callback=initMap" type="text/javascript"></script> 