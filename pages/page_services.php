<?php
require("../include/config.php");
require("../include/db.php");

echo "<h2>Services</h2>";

$sql = "SELECT * FROM services";
$result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

if (mysqli_num_rows($result) > 0) {
    echo <<<EOT
    <table class="table table-striped table-responsive">
        <tr class="table-success">
            <th scope="col">Service Name</th>
            <th scope="col">Service Description</th>
            <th scope="col">Service Price</th>
            <th scope="col">Service Image</th>
        </tr>
EOT;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        echo <<<EOT
            <tr>
                <td>$row[service_name]</td>
                <td>$row[service_description]</td>
                <td>$row[service_price]</td>
                <td><img src="img/services/$row[service_image]" title="$row[service_name]-slika" border="1" width="200"/></td>
            </tr>
EOT;
    }
    echo "</table><br>";
    mysqli_free_result($result);
}
mysqli_close($connection);
?>