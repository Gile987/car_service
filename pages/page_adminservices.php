
<?php
session_start();
require("../include/config.php");
require("../include/db.php");
require("../include/functions.php");

?>

<?php
//This page returns data only if the user is logged in
if (!isset($_SESSION['user_id']) && !isset($_SESSION['role_id']) && $_SESSION['role_id'] !== 1) {
  $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
  <h4 class=\"alert-heading\">Error!</h4>

  <p class=\"mb-0\">You do not have the permission to view this page!</p>

  </div>";
  header("location: page_message.php");
  exit();
}
else {
echo "<h2>Services</h2>";

$sql = "SELECT * FROM services";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0) {
  echo <<<EOT
  <div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role"tab" id="headingOne">
      <h6 class="mb-0">
        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls"collapseOne">Add New Service</a>
      </h6>
    </div>
    <a id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <form id="addService" method="POST" enctype="multipart/form-data">
          <label>Service Name</label><br>
          <input type="text" name="service_name" id="service_name" value="">
          <br><br>
          <label>Service Description</label><br>
          <input type="text" name="service_description" id="service_description" value="">
          <br><br>
          <label>Service Time</label><br>
          <input type="text" name="service_time" id="service_time" value="">
          <br><br>
          <label>Service Price</label><br>
          <input type="text" name="service_price" id="service_price" value="">
          <br><br>
          <label>Image:</label><br>
          <input type="file" name="file" id="image" required> <br>
          <button type="submit" class="btn btn-primary" id="newService" name="submit" >Submit</button>
        </form>
      </div>
    </a>
  </div>
  </div>
  <table class="table table-striped table-responsive">
    <tr class="table-success">
      <th scope="col">&nbsp</th>
      <th scope="col">Service Name</th>
      <th scope="col">Service Description</th>
      <th scope="col">Service Time</th>
      <th scope="col">Service Price</th>
      <th scope="col">Service Image</th>
      <th scope="col">&nbsp</th>
    </tr>
EOT;
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
      $row_id = $row["service_id"];
      $row_name = $row["service_name"];
      $row_description = $row["service_description"];
      $row_time = $row["service_time"];
      $row_price = $row["service_price"];
      $row_img = $row["service_image"];

      echo <<<PPT
        <tr id="$row_id">
          <td><button type="submit" class="btn btn-secondary btn-edit" id="$row_id" name="edit" >Edit</button></td>
          <td><input class="ser-input ser-size-name" id="name$row_id" name="ser_name" value="$row_name" ></td>
          <td id="hideMe"><input name="ser_rowid" value="$row_id"></td>
          <td><textarea class="ser-input" id="service_description$row_id" rows="5" name="ser_description">$row_description</textarea></td>
          <td><input class="ser-input ser-size" id="service_time$row_id"  name="ser_time" value="$row_time" ></td>
          <td><input class="ser-input ser-size" id="service_price$row_id"  name="ser_price" value="$row_price" ></td>
          <td><img src="img/services/$row_img" title="$row_name-slika" class="service-photo img-fluid"/></br>
          <input class="ser-input" id="image$row_id" type="file" name="ser_file" value="$row_img" ></td>
          <td><button type="submit" class="btn btn-secondary btn-delete" id="$row_id" name="edit" >Delete</button></td>
        </tr>        
PPT;

    }
    echo "</table><br>";
    mysqli_free_result($result);
}
mysqli_close($connection);
}
?>

<script>

$(document).ready(function() {
  // adding a new service
  $('#addService').submit(function (event) {
    event.preventDefault();
    var formData = new FormData(this);

    $.ajax({
      url: "adminaddservice.php",
      type: "post",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function (data) {
        alert(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
          alert('Internal Server Error: ' + jqXHR.responseText);
        } 
        else if (jqXHR.status == 404) {
          alert('Requested page not found: ' + jqXHR.responseText);
        }
        else {
          alert('Unexpected error.');
        }
      }
    });
  });
  // Editing a service
  $('.btn-edit').click(function (event) {
    event.preventDefault();
    var row = $(this).closest("tr");
    var formData = new FormData();

    formData.append("s_rowid",row.find('[name="ser_rowid"]').val());
    formData.append("s_name",row.find('[name="ser_name"]').val());
    formData.append("s_desc",row.find('[name="ser_description"]').val());
    formData.append("s_time",row.find('[name="ser_time"]').val());
    formData.append("s_price",row.find('[name="ser_price"]').val());
    formData.append("file",row.find('[name="ser_file"]')[0].files[0]);

    $.ajax({
      url: "adminupdateservices.php",
      type: "post",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function (data) {
        alert(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
          alert('Internal Server Error: ' + jqXHR.responseText);
        } 
        else if (jqXHR.status == 404) {
          alert('Requested page not found: ' + jqXHR.responseText);
        }
        else {
          alert('Unexpected error.');
        }
      }
    });
  });

  // deleting a service
  $('.btn-delete').click(function (event) {
    event.preventDefault();
    var row = $(this).closest("tr");
    var s_rowid = row.find('[name="ser_rowid"]').val();

    $.ajax({
      url: "admindeleteservice.php",
      type: "post",
      data: { s_rowid:s_rowid },
      dataType: "html",
      cache: false,
      success: function (data) {
        alert(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
          alert('Internal Server Error: ' + jqXHR.responseText);
        } 
        else if (jqXHR.status == 404) {
          alert('Requested page not found: ' + jqXHR.responseText);
        }
        else {
          alert('Unexpected error.');
        }
      }
    });
  });

});

</script>


