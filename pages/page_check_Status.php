<?php
session_start();
    
//This page returns data only if the user is logged in
if (!isset($_SESSION['user_id']) && !isset($_SESSION['role_id']) && $_SESSION['role_id'] !== 2) {
  $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
  <h4 class=\"alert-heading\">Error!</h4>

  <p class=\"mb-0\">You do not have the permission to view this page!</p>

  </div>";
    header("location: page_message.php");
    exit();
}

echo "<h2>STATUS</h2>";
echo "<p class=\"lead\">Enter the plate number of your vehicle in the text-field below to check the status of your vehicle. 
Please enter only numbers, letters, and spaces!</p>";
?>
<form id="search_platenum">
  <input type="textbox" value="" name="search" placeholder="Write here..." id="search" />   
  <input type="submit" value="Send" class="btn btn-cecondary "/>
</form>
<div id="display"></div>

<script>

$(document).ready(function () {
  // search for a plate number, and print out the necessary information
  $("#search_platenum").submit(function (event) {
    $("#display").empty();
    console.log(3);
    event.preventDefault();
    var plate_num = $("#search").val();
    console.log(plate_num);
    $.ajax({
      type: "post",
      url: "search_platenum.php",
      data: {
        plate_num: plate_num
      },
      dataType: 'html',
      cache:false,
      success: function (data) {
        json = JSON.parse(data);

        if (json.problems == undefined) {
          swal(data);
        }
        else {
          for(let i = 0; i<json.problems.length; i++) {
            if(json.problems[i].worker_comment==null) {
              json.problems[i].worker_comment='none';
            }
            var carStatus = $('#display');
            var list = $('<div id="plate_info" class="">').appendTo(carStatus);
            $(list).append('<p>Status of the submission with the plate number ' + json.problems[i].plate_num + ', received on: ' + json.problems[i].problem_date + ' is: ' + '<strong>' + json.problems[i].problem_status_id + '</strong>' + 
            '. ' + 'Price of our services is: ' + '<strong>' + json.problems[i].worker_total_price + '</strong>' + ' (The price is final when the status of your vehicle is <strong>finished.</strong>' + 
            ' We will infom you if the price changes.). ' + 'Our message (if any): ' + json.problems[i].worker_comment + '</p>');
          }
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
          alert('Internal Server Error: ' + jqXHR.responseText);
        } 
        else if (jqXHR.status == 404) {
          alert('Requested page not found: ' + jqXHR.responseText);
        }
        else {
          alert('Unexpected error.');
        }
      }
    });
  });
});

var url = "js/validation_form.js";
$.getScript(url);

</script>