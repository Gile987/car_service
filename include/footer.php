<!-- Footer -->
<footer>

    <!-- Footer Elements -->
    <div class="container">
    <div class="row">
      <div class="col-sm-12">

        <div class="row-footer">
          <div class="footer-nav text-center">
            <a class="btn btn-social-icon btn-google-plus" href="#">
              <i class="fa fa-google-plus"></i>
            </a>
            <a class="btn btn-social-icon btn-facebook" href="#">
              <i class="fa fa-facebook"></i>
            </a>
            <a class="btn btn-social-icon btn-linkedin" href="#">
              <i class="fa fa-linkedin"></i>
            </a>
            <a class="btn btn-social-icon btn-twitter" href="#">
              <i class="fa fa-twitter"></i>
            </a>
            <a class="btn btn-social-icon" href="#">
              <i class="fa fa-envelope-o"></i>
            </a>
          </div>

        </div>
      </div>
    </div>
    <!-- Footer Elements -->


    <div class="text-center">
      <a href="#pagetermsandconditions.php" target="_new"> Terms & Conditions</a>
      <br>
      <a href="#pageprivacypolicy.php" target="_new"> Privacy Policy</a>
    </div>

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="mailto:veresdamir@gmail.com?Subject=Hello">Damir</a><span> &</span>
      <a href="mailto:mladen.reljic87@gmail.com?Subject=Hello" target="_top" >Gile</a>
    </div>
    <!-- Copyright -->

  </footer>
  <script>

var url = "js/validation_form.js";
$.getScript(url);

/* 
Pops up an easter-egg after a user types "iddqd" on the keyboard
keyPressed is empty, until letters IDDQD are typed in that order, after which the elements with classes popup-overlay and popup-content receive a class "active", 
which makes the whole layer visible.
*/
$('body').bind('keydown', function(e) {
  if (typeof(keyPressed) == "undefined") {
		keyPressed = [];
		}
	var code = (e.keyCode ? e.keyCode : e.which);
 	keyPressed.push(code);
 	if (keyPressed.length === 5) {
	 	if (keyPressed[0] === 73 && keyPressed[1] === 68 && keyPressed[2] === 68 && keyPressed[3] === 81 && keyPressed[2] === 68){
      $('.popup-overlay, .popup-content').addClass("active");
	 		keyPressed = [];
	 		}
	 	else {
	 		keyPressed = [];
	 	}	
	 }
});
$(".closeMe, .popup").on("click", function(){
$(".popup-overlay, .popup-content").removeClass("active");
});

</script>
  <!-- Footer -->
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!-- <script src="js/jquery.min.js"></script> -->
  <!-- <script src="js/popper.min.js"></script> -->
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="js/tether.min.js"></script>
  <!-- <script src="js/bootstrap.min.js"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="js/index.js"></script>
  
  <!-- <script type="text/javascript" src="js/validation_form.js"></script>-->



  </body>
</html>