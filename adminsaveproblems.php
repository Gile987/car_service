<?php
require("include/config.php");
require("include/db.php");
require("include/functions.php");


if(isset($_POST["mail_opt"]) && isset($_POST["problem_id"]) && isset($_POST["user_email"]) && isset($_POST["user_name"]) && isset($_POST["problem_total_price"]) && isset($_POST["admin_comment"])&& isset($_POST["admin_price"])&& isset($_POST["status"])  ) {


    $mail_opt = mysqli_real_escape_string($connection, $_POST['mail_opt']);
    $problem_id  = mysqli_real_escape_string($connection, $_POST['problem_id']);
    $email = mysqli_real_escape_string($connection, $_POST['user_email']);
    $name = mysqli_real_escape_string($connection, $_POST['user_name']);
    $admin_comment = mysqli_real_escape_string($connection, $_POST['admin_comment']);
    $problem_total_price = mysqli_real_escape_string($connection, $_POST['problem_total_price']);
    $admin_price = mysqli_real_escape_string($connection, $_POST['admin_price']);
    $problem_status = mysqli_real_escape_string($connection, $_POST['status']);
    

    //sub totla price +added/discounted worker price
   $worker_total_price = $problem_total_price + $admin_price;
   

    $sql = "SELECT problem_status_id FROM problem_status WHERE status='$problem_status'";

    $result = mysqli_query($connection,$sql) or die(mysql_error($connection));
//if yes, that check if its in service or its done
while($row = $result->fetch_assoc()) {
        $status = $row['problem_status_id'];    
}
//IF PROBLEM IS FINISHED
if($status=="4") {

    $sql_upd = "UPDATE problems SET problem_status_id='$status', problem_status_update=NOW(), worker_comment='$admin_comment',worker_added_price='$admin_price',  worker_total_price='$worker_total_price', problem_finished=NOW() WHERE problem_id='$problem_id'";

    if($mail_opt=="send") {

        $message = "Your problem is finished <br><br>
        <p>Please contact us if you need more information</p>";
    
        $response = sendEmail($email, $name, $message);
    
        if ($response == 1) {
           
            if ($connection->query( $sql_upd) === TRUE) {
                echo json_encode( "You have successfully saved a problem and email is sent to user.");
                exit();
            } else {
                echo json_encode( "Email is sent but there was a error saving record: " . $connection->error);
            }
                
        }
  // if there was a error and email was not sent but data is saved
else {
    if ($connection->query( $sql_upd) === TRUE) {
        echo json_encode( "You have successfully saved a problem butemail was not sent to user.");
        exit();
    } else {
        echo json_encode( "Email was not sent and there was a error saving record: " . $connection->error);
    }
}

}
if ($connection->query($sql_upd) === TRUE) {
    echo json_encode( "You have successfully saved a problem.");
    exit();
} else {
    echo json_encode( "Error saving record: " . $connection->error);
}
}


//if problem status was just changed
else {

    $sql_upd = "UPDATE problems SET problem_status_id='$status', problem_status_update=NOW(), worker_comment='$admin_comment',worker_added_price='$admin_price',  worker_total_price='$worker_total_price' WHERE problem_id='$problem_id'";


    if($mail_opt=="send") {

        $message = "Your problem status is changed to $problem_status<br><br>
        <p>Please contact us if you need more information</p>";
    
        $response = sendEmail($email, $name, $message);
    
        if ($response == 1) {
       
            if ($connection->query( $sql_upd) === TRUE) {
                echo json_encode( "You have successfully saved a problem and email is sent to user.");
                exit();
            } else {
                echo json_encode( "Email is sent but there was a error saving record: " . $connection->error);
            }
                
        }
    // if there was a error and email was not sent but data is saved
    
    else {
        if ($connection->query( $sql_upd) === TRUE) {
            echo json_encode( "You have successfully saved a problem butemail was not sent to user.");
            exit();
        } else {
            echo json_encode( "Email was not sent and there was a error saving record: " . $connection->error);
        }
    }
    
    }
    if ($connection->query($sql_upd) === TRUE) {
        echo json_encode( "You have successfully saved a problem.");
        exit();
    } else {
        echo json_encode( "Error saving record: " . $connection->error);
    }
    }
}
else{
    echo json_encode( "Error");
    exit();
}  
  ?>