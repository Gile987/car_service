<?php
/* Reset your password form, sends reset.php password link */
session_start();
require("include/config.php");
require("include/functions.php");
require("include/db.php");
require("include/gump.class.php");

if (!isset($_POST['forget'])) {
    header("location: index.php");
} 
else {

    $validator = new GUMP();

    $email = mysqli_real_escape_string($connection, $_POST['email']);

    $_POST = array(
        'email' => $email
    );

    $_POST = $validator->sanitize($_POST);

    $rules = array(
        'email' => 'required|valid_email|min_len,3|max_len,32'
    );

    $filters = array(
        'email'	=> 'trim|sanitize_email',
    );

    $_POST = $validator->filter($_POST, $filters);

    $validated = $validator->validate(
        $_POST, $rules
    );
    
    if($validated === TRUE){

        $sql_ver = "SELECT CONCAT(firstname,' ',lastname) AS name FROM users
        WHERE email = '$email' AND active=1";

        $result = mysqli_query($connection, $sql_ver) or die(mysql_error($connection));

        if ($result->num_rows == 0) {
            $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
            <h4 class=\"alert-heading\">Error!</h4>
            <p>User with this email does not exist, or the account hasn't been verified.</p>

            </div>";
            header("location: index.php?#pagemessage.php");
        } else { // Email doesn't already exist in a database, proceed...
            while ($record = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                $_SESSION['name'] = $record['name'];
            }

            $name = $_SESSION['name'];
            $verification_temp = MD5(str_shuffle(SALT1));
            $verification_code = substr($verification_temp, 0, 10);

            // active is 0 by DEFAULT (no need to include it here)

            $sql = "UPDATE users SET verification_code='$verification_code', verification_time=NOW()+INTERVAL 20 MINUTE WHERE email='$email'";

            // sending email with function

            $message = "Please click below: <br /><br />
            <a href='http://localhost/auto3/src/reset.php?email=$email&verification_code=$verification_code'>Klikni ovde</a>";
            $response = sendEmail($email, $name, $message);

            // if email is sent 

            if ($response == 1) {
                $_SESSION['message'] = "<div class=\"alert alert-info\" role=\"alert\">
                <p>If the address exists in the database, then the email has been sent to it! </p>

                <p class=\"mb-0\">Please verify your account by clicking on the link in the message!</p>

                </div>";
                header("location: index.php?#pagemessage.php");
            }

            // if there was a error and email was not sent

            else {
                $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
                <h4 class=\"alert-heading\">Error!</h4>
                <p>Sorry, something went wrong. Please try again later..</p>

                </div>";
                header("location: index.php?#pagemessage.php");
            }

            if ($connection->query($sql) === true) {
                header("location: index.php?#pagemessage.php");
            } 
            else {
                $_SESSION['message'] = "<div class=\"alert alert-danger\" role=\"alert\">
                <h4 class=\"alert-heading\">Error!</h4>
                <p>Error: ' . $sql . '<br />' . $connection->error.</p>

                </div>";
                header("location: index.php?#pagemessage.php");
            
            }

            $connection->close();
        }
    }
    else {
        echo $validator->get_readable_errors(true);
    }
}
?>