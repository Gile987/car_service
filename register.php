<?php
session_start();
require("include/config.php");
require("include/db.php");
require("include/functions.php");
require("include/gump.class.php");


if (!isset($_POST['register'])) {
    header("location: index.php");
}
else{
    if(isset($_POST['g-recaptcha-response'])) {
        $captcha = $_POST['g-recaptcha-response'];
    }
if(!$captcha) {
    $_SESSION['message'] = "<h1>Error</h1><br><p>Please make sure that you are not a robot!<p>";
    header("location: index.php?#pagemessage.php");
}
else{

    $validator = new GUMP();

    // Escape all $_POST variables to protect against SQL injections
    $first_name = mysqli_real_escape_string($connection, $_POST['firstname']);
    $last_name = mysqli_real_escape_string($connection, $_POST['lastname']);
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $phone_num = mysqli_real_escape_string($connection, $_POST['phonenum']);
    $conf_email = mysqli_real_escape_string($connection, $_POST['conf_email']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);
    $conf_password = mysqli_real_escape_string($connection, $_POST['conf_password']);

    $_POST = array(
        'firstname'   => $first_name,
        'lastname' 	  => $last_name,
        'email'	      => $email,
        'phonenum'    => $phone_num,
        'confemail'   => $conf_email,
        'password'	  => $password,
        'confpassword'  => $conf_password
    );

    $_POST = $validator->sanitize($_POST);

    $rules = array(
        'firstname'   => 'required|alpha|min_len,3|max_len,15',
        'lastname' 	  => 'required|alpha|min_len,3|max_len,15',
        'email'	      => 'required|valid_email|min_len,3|max_len,32',
        'phonenum'    => 'required|min_len,16|max_len,17',
        'confemail'   => 'required|valid_email|min_len,3|max_len,32',
        'password'	  => 'required|min_len,6|max_len,20',
        'confpassword'  => 'required|min_len,6|max_len,20'
    );

    $filters = array(
        'firstname'   => 'trim|sanitize_string',
        'lastname' 	  => 'trim|sanitize_string',
        'email'	      => 'trim|sanitize_email',
        'phonenum'    => 'trim|sanitize_string',
        'confemail'   => 'trim|sanitize_string',
        'password'	  => 'trim|sanitize_string',
        'confpassword'  => 'trim|sanitize_string'
    );

    $_POST = $validator->filter($_POST, $filters);

    $validated = $validator->validate(
        $_POST, $rules
    );

    if($validated === TRUE){

        $sql_ver = "SELECT * FROM users WHERE email='$email'";

        $result = mysqli_query($connection,$sql_ver) or die(mysql_error());
        
        if ($result->num_rows > 0) {
            
            $_SESSION['message'] = "<h1>Error</h1><br><p>Korisnik sa ovim emailom vec postoji!<p>";
        
            header("location: index.php?#pagemessage.php");
        }

        else { // Email doesn't already exist in the database, proceed...
            $password_temp = SALT1."$password".SALT2;

            $password_hash = MD5($password_temp);
            $verification_temp = MD5(str_shuffle(SALT1));
            $verification_code = substr($verification_temp, 0, 10);

            // active is 0 by DEFAULT (no need to include it here)
            $sql = "INSERT INTO users (firstname, lastname, phone, email, password, verification_code, verification_time) VALUES ('$first_name','$last_name','$phone_num', '$email','$password_hash', '$verification_code',NOW()+INTERVAL 1 DAY)";

            $name = $first_name.''.$last_name;

            $message = "Please click below: <br><br>
            <a href='http://localhost/auto3/src/verify.php?email=$email&verification_code=$verification_code'>Klikni ovde</a>";

            $response = sendEmail($email, $name, $message);

            if ($response == 1) {
            
                $_SESSION['message'] = "<div class=\"alert alert-success\" role=\"alert\">
                <h4 class=\"alert-heading\">Success!</h4>
                <p>Confirmation link is sent to $email! </p>

                <p class=\"mb-0\">Please verify your account by clicking on the link in the message!</p>

                </div>";
                header("location: index.php?#pagemessage.php");

            }
            // if there was a error and email was not sent 
            else {
                $_SESSION['message'] = "<div class=\"alert alert-warning\" role=\"alert\">
                <h4 class=\"alert-heading\">Error!</h4>
                <p>Sorry, something went wrong. Please register again</p>

                </div>";
                header("location: index.php?#pagemessage.php");
                exit();//stops the process
            }


            if ($connection->query($sql) === TRUE) {
            // echo "New record created successfully";
                header("location: index.php?#pagemessage.php"); 
            } else {
                $_SESSION['message'] = "<div class=\"alert alert-danger\" role=\"alert\">
                <h4 class=\"alert-heading\">Error!</h4>
                <p>Error: ' . $sql . '<br />' . $connection->error.</p>

                <p class=\"mb-0\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>

                </div>";
                header("location: index.php?#pagemessage.php");     
            }
            
            $connection->close();
        }
    }
    else{
        echo $validator->get_readable_errors(true);
    }
}
}
    ?>