-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 28, 2018 at 06:07 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autoservis`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE IF NOT EXISTS `cars` (
  `cars_id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `plate_num` varchar(16) NOT NULL,
  `car_brand` varchar(16) NOT NULL,
  `car_model` varchar(16) NOT NULL,
  `car_year` varchar(4) NOT NULL,
  PRIMARY KEY (`cars_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`cars_id`, `problem_id`, `plate_num`, `car_brand`, `car_model`, `car_year`) VALUES
(1, 1, 'SU 1111', 'MB', 'CL600', '2003'),
(2, 2, 'SU AB 123', 'AUDI', 'A3', '2008'),
(3, 3, 'SU 123 AB', 'BMW', '750IL', '2009'),
(4, 4, 'SU VD 542', 'ZSV', '128', '1984');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

DROP TABLE IF EXISTS `problems`;
CREATE TABLE IF NOT EXISTS `problems` (
  `problem_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) DEFAULT NULL,
  `problem_status_id` int(2) NOT NULL DEFAULT '1',
  `problem_date` timestamp NOT NULL,
  `problem_status_update` timestamp NULL DEFAULT NULL,
  `user_comment` text,
  `worker_comment` text,
  `problem_total_price` float DEFAULT NULL,
  `worker_added_price` float DEFAULT '0',
  `problem_total_time` time DEFAULT NULL,
  `worker_total_price` float DEFAULT NULL,
  `problem_finished` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`problem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`problem_id`, `user_id`, `car_id`, `problem_status_id`, `problem_date`, `problem_status_update`, `user_comment`, `worker_comment`, `problem_total_price`, `worker_added_price`, `problem_total_time`, `worker_total_price`, `problem_finished`) VALUES
(1, 7, 1, 2, '2018-07-28 11:04:55', '2018-07-28 18:03:45', 'Pls halp', ' Thank you. ', 6000, 0, '02:00:00', 6000, NULL),
(2, 4, 2, 2, '2018-07-28 17:37:13', '2018-07-28 17:52:27', 'I need a couple of filters to be replaced.', ' Your problem is confirmed. We will update you for further details.\r\nYour final price will be discounted with 500 units.', 3500, -500, '01:00:00', 3000, NULL),
(3, 4, 3, 2, '2018-07-28 17:38:36', '2018-07-28 18:04:52', 'There is a couple of problems with my car. ', ' Due to the number of problems we need to charge you extra 2000 units to the final price.', 11750, 2000, '06:00:00', 13750, NULL),
(4, 3, 4, 1, '2018-07-28 18:06:11', NULL, 'My old car need a service.', NULL, 7100, 0, '05:00:15', 7100, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `problem_reservation`
--

DROP TABLE IF EXISTS `problem_reservation`;
CREATE TABLE IF NOT EXISTS `problem_reservation` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `reservation_date` date NOT NULL,
  `reservation_start` time NOT NULL,
  `reservation_end` time NOT NULL,
  PRIMARY KEY (`reservation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problem_reservation`
--

INSERT INTO `problem_reservation` (`reservation_id`, `problem_id`, `reservation_date`, `reservation_start`, `reservation_end`) VALUES
(1, 1, '2018-08-01', '10:30:00', '12:30:00'),
(2, 2, '2018-07-30', '09:00:00', '10:00:00'),
(3, 3, '2018-07-31', '10:00:00', '16:00:00'),
(4, 4, '2018-08-03', '10:30:00', '16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `problem_service`
--

DROP TABLE IF EXISTS `problem_service`;
CREATE TABLE IF NOT EXISTS `problem_service` (
  `problem_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `problem_service_price` float NOT NULL,
  `problem_service_time` time NOT NULL,
  PRIMARY KEY (`problem_service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problem_service`
--

INSERT INTO `problem_service` (`problem_service_id`, `problem_id`, `service_id`, `problem_service_price`, `problem_service_time`) VALUES
(1, 1, 3, 1500, '00:30:00'),
(2, 1, 5, 4500, '01:30:00'),
(3, 2, 2, 2000, '00:30:00'),
(4, 2, 3, 1500, '00:30:00'),
(5, 3, 5, 4500, '01:30:00'),
(6, 3, 7, 2500, '01:15:00'),
(7, 3, 8, 1250, '01:15:00'),
(8, 3, 18, 3500, '02:00:00'),
(9, 4, 1, 1600, '02:30:15'),
(10, 4, 2, 2000, '00:30:00'),
(11, 4, 18, 3500, '02:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `problem_status`
--

DROP TABLE IF EXISTS `problem_status`;
CREATE TABLE IF NOT EXISTS `problem_status` (
  `problem_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`problem_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problem_status`
--

INSERT INTO `problem_status` (`problem_status_id`, `status`) VALUES
(1, 'received'),
(2, 'accepted/in process'),
(3, 'refused'),
(4, 'finished'),
(5, 'returned');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `privilage` varchar(32) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `privilage`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'worker');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(64) NOT NULL,
  `service_description` text NOT NULL,
  `service_time` time NOT NULL,
  `service_price` float NOT NULL,
  `service_image` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_description`, `service_time`, `service_price`, `service_image`) VALUES
(1, 'Replace Oil Filter', 'We replace your old oil filter with a new one.', '02:30:15', 1600, 'oil_filter.jpg'),
(2, 'Replace Air Filter', 'We replace your old air filter with a new one.', '00:30:00', 2000, 'air_filter.jpg'),
(3, 'Replace Fuel Filter', 'We replace your old fuel filter with a new one.', '00:30:00', 1500, 'fuel_filter.jpg'),
(4, 'Replace Tires', 'We replace your old tires with new ones.', '01:15:15', 3000, 'car_tires.jpg'),
(5, 'Engine Tuning', 'We tune your engine to make it perform at its fullest potential.', '01:30:00', 4500, 'car_engine_tune.jpg'),
(6, 'Lubricate Components', 'We will lubricate all of the necessary components of your vehicle.', '00:45:00', 2000, 'lubrication.jpg'),
(7, 'Brake Disc Replacement', 'We replace your old brake disc with a new one.', '01:15:00', 2500, 'car_brake.jpg'),
(8, 'Car Wash', 'We wash your entire car and clean the interior. ', '01:15:00', 1250, 'wash.jpg'),
(18, 'Check for ECU Errors', 'We will check your ECU for errors and take corrective actions', '02:00:00', 3500, 'ecu.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(24) NOT NULL,
  `lastname` varchar(24) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(1) NOT NULL DEFAULT '2',
  `verification_code` varchar(32) NOT NULL,
  `verification_time` timestamp NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `phone`, `email`, `password`, `active`, `role_id`, `verification_code`, `verification_time`) VALUES
(1, 'Panta', 'Pantic', '+381-25-9879-659', 'panta@test.com', 'bb245bc19c05c853d26b3ceb0be946b2', 1, 1, '', '2018-07-17 21:05:16'),
(2, 'Marko', 'Markovic', '+381-65-9874-989', 'marko@test.com', 'bb245bc19c05c853d26b3ceb0be946b2 	', 1, 1, '', '2018-07-25 13:24:25'),
(3, 'Jovana', 'Jovanovic', '+381-65-9874-989', 'jovana@test.com', 'bb245bc19c05c853d26b3ceb0be946b2', 1, 2, '', '2018-07-19 18:55:57'),
(4, 'Petar', 'Petrovic', '+381-25-6548-896', 'petar@test.com', 'bb245bc19c05c853d26b3ceb0be946b2', 1, 2, '', '2018-07-23 22:14:07'),
(7, 'Mladen', 'Reljic', '+381-65-2405-111', 'mladen.reljic87@gmail.com', '019fabae245f53fc7df5d97d764d895a', 1, 2, '', '2018-07-27 19:36:55'),
(9, 'Deda', 'Mraz', '+381-65-2405-222', 'nyarlathotep_stgt@hotmail.com', 'bb245bc19c05c853d26b3ceb0be946b2', 1, 2, '', '2018-07-28 21:14:31');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
