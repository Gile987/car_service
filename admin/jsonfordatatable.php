<?php

session_start();
//only if admin is loged in this page will return data to page check new problems
if (!isset($_SESSION['user_id']) && !isset($_SESSION['role_id']) && $_SESSION['role_id'] !== 1) 
{

    header("location: ../index.php");
}
else
{ 

require("../include/config.php");
require("../include/db.php");
//require("include/functions.php");

$problem_status_id= mysqli_real_escape_string($connection, $_GET['problem_status_id']);

$_SESSION['problem_status_id'] = $problem_status_id;
$sql_status="SELECT * FROM problem_status";

$result = mysqli_query($connection,$sql_status) or die(mysql_error($connection));

if ($result->num_rows>0) {
//$problem_status = array();

while($row = $result->fetch_assoc()) {
   // $problem_status[] = $row['status'];
   $problem_status[$row['problem_status_id']] = $row['status'];
   //return $problem_status;
  
}

}
//print_r ($problem_status);
if ($problem_status_id == 1) {

$sql="SELECT problems.problem_id, CONCAT (firstname,' ', lastname) as name, phone, email,plate_num,car_brand,car_model,car_year,problem_date,status,problem_status_update,user_comment, worker_comment,problem_total_price, worker_added_price,problem_total_time,worker_total_price,problem_finished, reservation_date,reservation_start, reservation_end FROM problems INNER JOIN users ON problems.user_id = users.user_id INNER JOIN cars ON problems.problem_id = cars.problem_id INNER JOIN problem_status ON problems.problem_status_id = problem_status.problem_status_id INNER JOIN problem_reservation ON problems.problem_id=problem_reservation.problem_id WHERE problems.problem_status_id = '$problem_status_id'";
}
else {
    $sql="SELECT problems.problem_id, CONCAT (firstname,' ', lastname) as name, phone, email,plate_num,car_brand,car_model,car_year,problem_date,status,problem_status_update,user_comment, worker_comment,problem_total_price, worker_added_price,problem_total_time,worker_total_price,problem_finished, reservation_date,reservation_start, reservation_end FROM problems INNER JOIN users ON problems.user_id = users.user_id INNER JOIN cars ON problems.problem_id = cars.problem_id INNER JOIN problem_status ON problems.problem_status_id = problem_status.problem_status_id INNER JOIN problem_reservation ON problems.problem_id=problem_reservation.problem_id";
}


$result = mysqli_query($connection,$sql) or die(mysql_error($connection));

//if belongs to loged user than make JSOn file and send it back to AJAX
if ($result->num_rows>0) {
    $json_response = array();
    while($row = $result->fetch_assoc()) 
        {
            
                
            $json_response[ "rows"][] = array('problem_id'=> $row['problem_id'], 
            'name'=> $row['name'],'phone'=> $row['phone'],'pemail'=> $row['email'],
            'plate_num'=> $row['plate_num'],'car_brand'=> $row['car_brand'],'car_model'=> $row['car_model'],'car_year'=> $row['car_year'],'problem_date'=> $row['problem_date'],
            'status'=> $row['status'], 'problem_status_update'=> $row['problem_status_update'],'user_comment'=> $row['user_comment'],'worker_comment'=> $row['worker_comment'],'problem_total_price'=> $row['problem_total_price'], 'worker_added_price'=> $row['worker_added_price'],'problem_total_time'=> $row['problem_total_time'],'worker_total_price'=> $row['worker_total_price'], 'problem_finished'=> $row['problem_finished'],'reservation_date'=> $row['reservation_date'],'reservation_start'=> $row['reservation_start'], 'reservation_end'=> $row['reservation_end'], 'problem_status' => $problem_status,
        );

            
    }

        //aditional making of JSOn file with data from DB
        $json = fopen('../json/problems_data.json', 'w');
fwrite($json, json_encode($json_response,JSON_PRETTY_PRINT));
fclose($json);
header("location: ../index.php?#pagechecknewproblems.php");

exit();
}   
else {
    $_SESSION['message'] ="<div class=\"alert alert-warning\" role=\"alert\">
    <h4 class=\"alert-heading\">Warning!</h4>
    <p>There are no problems in the database.</p>

    <p class=\"mb-0\">Try again later.</p>

  </div>";

header("location: ../index.php?#pagemessage.php");
exit();
}    
}
?>