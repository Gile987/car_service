$(document).ready(function() {

    jQuery.validator.addMethod("phoneserbia", function(phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, ""); 
        return this.optional(element) || phone_number.length > 8 &&
            phone_number.match(/^(\+381)?[-]([0-9]{2})[-]?([0-9]{4})[-]?([0-9]{3,4})$/);
            //(/^(\+381)?[-]?([0-9]){2})[-]?([0-9]{4})[-]?([0-9]{3})$/);
    }, "Please specify a valid phone number");
    //", #formlogin
    jQuery.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z0-9\s]+$/);
     }, "Must contain only letters, numbers, or space.");
    
    $('.formvalidate').each( function(){
        var formval = $(this);
        formval.validate({
       
        rules: {
            firstname: {
                minlength: 3,
                required: true,
                maxlength: 20
                },
    
            lastname: {
                minlength: 3,
                required: true,
                maxlength: 20
                },
                phonenum: {   
                required: true,
                phoneserbia: true,
                },
            email: {
                required: true,
                email:true,
            },
            email1: {
                required: true,
                email:true,
            },
            email2: {
                required: true,
                email:true,
            },
            conf_email: {
                required: true,
                email: true,
                equalTo: '#email',
            },
            password: {
                minlength: 6,
                required: true,
                maxlength: 20
            },
            password1: {
                minlength: 6,
                required: true,
                maxlength: 20
            },
            conf_password: {
                required: true,
                maxlength: 20,
                equalTo: '#password',
            },
            plate_num: {
                required: true,
                minlength: 5,
                maxlength: 10,
                alpha:true
            },
            brand: {
                required: true
            },
            model: {
                required: true
            },
            car_year: {
                required: true
            },
            user_comment:{
                required: true,
                maxlength: 1000
            },
            search_platenum: {
                required: true
            },
            contact_text :{
                required: true,
                maxlength: 1000
            }
        },
        messages: {
            firstname: {
                minlength: jQuery.validator.format("Min {0} characters!"), 
                required: "Please enter your name!",  
            },
            lastname: {
                minlength: jQuery.validator.format("Min {0} characters!"),
                required: "Please enter your last name!",
            },
            
            email: {
                required: "Please enter your email address!",
                email: "Your email address must be in the format of name@domain.com",
            },
            email1: {
                required: "Please enter your email address!",
                email: "Your email address must be in the format of name@domain.com",
            },
            email2: {
                required: "Please enter your email address!",
                email: "Your email address must be in the format of name@domain.com",
            },
            conf_email:{
                required: "Please repeat your email address!",
                email: "Your email address must be in the format of name@domain.com",
                equalTo: "Your email doesn't match",
            },
            password: {
                minlength: jQuery.validator.format("Min {0} caracter!"),
                required: "Please enter your password!",
            },
            password1: {
                minlength: jQuery.validator.format("Min {0} caracter!"),
                required: "Please enter your password!",
            },
            conf_password: {
                required: "Please repeat your password!",
                equalTo: "Your password doesn't match",
            },
            cuser_comment: {
                required: "Please enter a message!",
                maxlength: jQuery.validator.format("Max {0} character!")
            },
            contact_text: {
                required: "Please enter a message!",
                maxlength: jQuery.validator.format("Max {0} character!")
            }
        }
    });
    });
    });