var default_content = "";

$(document).ready(function() {
    checkURL();
    $('.mr-auto li').click(function(e) {
        checkURL(this.hash);
        $('.mr-auto li').removeClass('active');
        $(this).addClass('active');
    });

    //filling in the default content
    default_content = $('#pageContent').html();
    setInterval("checkURL()", 50);
});

var lasturl = "";

function checkURL(hash) {
    if (!hash)
        hash = window.location.hash;
    if (hash != lasturl) {
        lasturl = hash;
        if (hash == "")
            $('#pageContent').html(default_content);
        else
            loadPage(hash);
    }
}

function loadPage(url) {
  url = url.replace('#page', '');
  $('#pageContent').animate({
      opacity: 0.1
  }, 1000);

  $.ajax({
    type: "POST",
    url: "load_page.php",
    data: 'page=' + url,
    dataType: "html",
    success: function(data) {
      if (parseInt(data) != 0) {
        $('#pageContent').load(data);

        if (data == "pages/page_checknewproblems.php") {
          $('.jumbotron').addClass('remove');
          $('.jumbotron').removeClass('show');
          $('#newproblems').removeClass('animated');
          $('.badge').removeClass('badge-primary');
          $('.badge').addClass('badge-success');
        }
        console.log(data);
        var data_title = data.substr(11).slice(0, -4);
        var data_title_upper = data_title.charAt(0).toUpperCase() + data_title.substr(1);
        document.title = data_title_upper;

        $('#pageContent').animate({
            opacity: 1
        }, 1000);
      }
    }
  });
}

$(function() {
  // User login
  $(".openit_forgot").on("click", function() {
    $(".formvalidate").addClass("remove");
    $(".formforget").removeClass("remove");
    $(".formforget").addClass("show");
  });

  $("#dropdownMenu1").on("click", function() {
    $(".formvalidate").removeClass("remove");
    $(".formforget").removeClass("show");
    $(".formforget").addClass("remove");
  });
});

// Hide the collapsed navbar after a user clicks on a link
$('.navbar-nav>li>a').on('click', function() {
  $('.navbar-collapse').collapse('hide');
});



var screen = $(window);
if (screen.width() < 768) {
  $('.navi-hider').click(function() {
    $('#links').toggle('visible');
  });
};