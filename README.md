Project - Car Workshop

Goal of the project: Practice everything learned in the past 6 months.

Following technologies and programming languages MUST be used: HTML, CSS, 
JavaScript, PHP, MySQL, AJAX, Jquery, Responsive Web Design, Animate.css.

Task:
- Home Page
- About Us page (text + photos)
- Prices of services
- Contact page (contact form + google map)
- Our Team (text + photos)
- Registration of users (web form with Captcha)
- Login Form
- Forgotten Password Form
- Check the status of the vehicle page (for users)

The user must receive an email after registering, and only after clicking on 
the link in the email is the registration process finalized, and the user can 
then log in.

The user is supposed to use a web form to inform the admin of the Car Workshop 
about the problems with the vehicle and make an appointment by selecting a date 
and time in the calendar. User must enter his/her plate number, problem 
description, choose a service, and other car-related info.

When a user reports a problem, the admin can see a message informing him of 
new problems. The admin can also choose to accept the tast ok reject it, and 
also inform the user by sending a message. Admin has the power to finalize the 
price, as well as the time of services.

The admin can also add new services, edit the old ones, as well as deleting 
any of them.


Extra technologies used: Bootstrap4, Datatable, FullCalendar, SweetAlert, 
Gulp, Wixel/GUMP, PHPMailer, SASS.

