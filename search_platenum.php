<?php

session_start();

require("include/config.php");
require("include/db.php");
require("include/gump.class.php");
  
if(!isset($_POST['plate_num'])) {
    header("location: index.php");
}
if(empty($_POST['plate_num'])) {
 //if plate number are not entered
    echo json_encode("Please enter your plate number!");
}
else{
    $validator = new GUMP();
    $plate_num= mysqli_real_escape_string($connection, $_POST['plate_num']);
    $user_id = mysqli_real_escape_string($connection, $_SESSION['user_id']);



    $_POST = array(
    'platenum'   => $plate_num,
    'userid' 	  => $user_id
    );

    $_POST = $validator->sanitize($_POST);

    $rules = array(
       'platenum'   => 'required|alpha_space|min_len,5|max_len,10',
        'userid' 	  => 'required|min_len,1|max_len,3'
    );

    $filters = array(
        'platenum'   => 'trim|sanitize_string',
        'userid' 	  => 'trim|sanitize_string'
    );

    $_POST = $validator->filter($_POST, $filters);

    $validated = $validator->validate(
        $_POST, $rules
    );

    if($validated === TRUE){

        header("Content-type: application/json; charset=utf-8'");
        if(!empty($plate_num)) {
            
            //check if car with that plate num exist in DB
            $sql = "SELECT * from cars WHERE plate_num='$plate_num'";
            $result = mysqli_query($connection,$sql) or die(mysql_error($connection));

            if ($result->num_rows>0) {
                $problem_exist = array();
                while($row = $result->fetch_assoc()) {
                    $car_problem_exist[] = $row['cars_id'];    
                }
                //change from array to string
                $car_problem_exist = implode(", ", $car_problem_exist);
                
                // if it exist checking if it belongs to loged user
                $sql_exist = "SELECT car_id,problem_date,user_comment,worker_comment,problem_total_price,worker_added_price,problem_total_time,worker_total_price,problem_status.status,plate_num,car_brand,car_model,car_year FROM problems INNER JOIN problem_status ON problems.problem_status_id=problem_status.problem_status_id LEFT JOIN cars ON problems.problem_id=cars.problem_id  WHERE user_id=$user_id AND car_id IN ($car_problem_exist)";


                $result = mysqli_query($connection,$sql_exist) or die(mysql_error($connection));
                //if belongs to loged user than make JSOn file and send it back to AJAX
                if ($result->num_rows>0) {
                    $json_response = array();
                    while($row = $result->fetch_assoc()) {
                        $json_response[ 'problems' ][] = array('car_id'=> $row['car_id'], 'problem_date'=> $row['problem_date'], 'plate_num'=> $row['plate_num'],'car_brand'=> $row['car_brand'],'car_model'=> $row['car_model'],'car_year'=> $row['car_year'],'problem_status_id'=> $row['status'], 'user_comment'=> $row['user_comment'],'worker_comment'=> $row['worker_comment'],'problem_total_price'=> $row['problem_total_price'], 'worker_added_price'=> $row['worker_added_price'],'problem_total_time'=> $row['problem_total_time'],'worker_total_price'=> $row['worker_total_price']);
                    }
                    //aditional making of JSOn file with data from DB
                    $json = fopen('json/response.json', 'w');
                    fwrite($json, json_encode($json_response,JSON_PRETTY_PRINT));
                    fclose($json);
                    //returning data to ajax call
                    echo json_encode($json_response, JSON_PRETTY_PRINT);
                    exit();
                }  //if plate number dont belong to loged user
                else {
                    echo json_encode("This plate number doesn't belong to you!");
                }    
            }
            //if plate numer are not in our DB
            else {
                echo json_encode("This plate number isn't in the database!");
            } 
        }
       
    }
    else{
        echo $validator->get_readable_errors(true);
    }
}

?>
